#!/bin/bash

EXTERNAL_INTERFACE="ens18"
INTERNAL_INTERFACE="ens19"
INTERNAL_IP="172.16.10.1"
DHCP_RANGE="172.16.10.2,172.16.10.200,255.255.255.0,2h"

SYSLINUX_URL="https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.zip"
AUTOINSTALL_URL="https://gitlab.com/devops_includes/autoinstall/ubuntu-server-24"
ISO_URL="https://mirrors.xtom.ee/ubuntu-releases/24.04/ubuntu-24.04.2-live-server-amd64.iso"

apt install apache2 wget git dnsmasq unzip tftpd-hpa -y
DEBIAN_FRONTEND=noninteractive apt install iptables-persistent -y

mkdir -p /srv/tftp/bios/boot/casper

cp -v ./configs/dnsmasq.conf /etc/dnsmasq.conf
sed -i "s/<INTERNAL_INTERFACE>/${INTERNAL_INTERFACE}/g" /etc/dnsmasq.conf
sed -i "s/<DHCP_RANGE>/${DHCP_RANGE}/g" /etc/dnsmasq.conf
sed -i "s/<INTERNAL_IP>/${INTERNAL_IP}/g" /etc/dnsmasq.conf

systemctl stop systemd-resolved tftpd-hpa
systemctl restart dnsmasq
if [[ $? -ne 0 ]]
then
  echo "Some error occurred during restarting dnsmasq service!"
  exit 1
fi
systemctl start systemd-resolved

mkdir /srv/tftp/bios/pxelinux.cfg/
cp -v ./configs/default /srv/tftp/bios/pxelinux.cfg/
sed -i "s/<INTERNAL_IP>/${INTERNAL_IP}/g" /srv/tftp/bios/pxelinux.cfg/default

wget ${SYSLINUX_URL}
unzip syslinux-6.03.zip -d syslinux
cp -v ./syslinux/bios/com32/elflink/ldlinux/ldlinux.c32 /srv/tftp/bios
cp -v ./syslinux/bios/com32/libutil/libutil.c32 /srv/tftp/bios
cp -v ./syslinux/bios/com32/menu/{vesa,}menu.c32 /srv/tftp/bios
cp -v ./syslinux/bios/core/{l,}pxelinux.0 /srv/tftp/bios
rm -rf syslinux

mkdir -p /var/www/html/ubuntu-24/server/
git clone ${AUTOINSTALL_URL} /var/www/html/ubuntu-24/server/autoinstall

wget ${ISO_URL} -O ubuntu.iso
cp -v ubuntu.iso /var/www/html/ubuntu-24/server/
mount ubuntu.iso /mnt
cp -v /mnt/casper/{vmlinuz,initrd} /srv/tftp/bios/boot/casper/
umount /mnt
rm -f ubuntu.iso

iptables -t nat -A POSTROUTING -o ${EXTERNAL_INTERFACE} -j MASQUERADE
iptables -A FORWARD -i ${INTERNAL_INTERFACE} -j ACCEPT
sysctl -a|grep "net.ipv4.ip_forward = 1" > /dev/null
if [[ $? -ne 0 ]]
then
  echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf 
  sysctl -p
fi

iptables-save > /etc/iptables/rules.v4

echo -e "\nInstallation Finished!"
